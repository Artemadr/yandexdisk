<?php

namespace App\Http\Controllers;

use App\Http\Requests\File\CheckApiRequest;
use App\Http\Requests\File\DownloadRequest;
use App\Http\Requests\File\UpdateRequest;
use App\Http\Requests\File\UploadRequest;
use App\Interfaces\ApiFilesInterface;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;

class FilesController extends Controller
{
    public function index(Request $request, ApiFilesInterface $filesService): array|Application|ResponseFactory|Response
    {
        $page = max(intval($request->page), 1);
        $perPage = intval(config('app.files_per_page'));

        try {
            $files = $filesService->getFiles($page, $perPage);
        } catch (Exception $e) {
            return response([
                'message' => $e->getMessage()
            ], 400);
        }

        return $files;
    }

    public function store(UploadRequest $request, ApiFilesInterface $filesService): array|Application|ResponseFactory|Response
    {
        $file = $request->validated('file');

        try {
            $filesService->uploadFile($file);
        } catch (Exception $e) {
            return response([
                'message' => $e->getMessage()
            ], 413);
        }

        return ['message' => 'Файл успешно загружен.'];
    }

    public function delete(DownloadRequest $request, ApiFilesInterface $filesService): array|Application|ResponseFactory|Response
    {
        $filePath = $request->validated('id');

        try {
            $apiResult = $filesService->deleteFile($filePath);
        } catch (Exception $e) {
            return response([
                'message' => $e->getMessage()
            ], 400);
        }

        if ($apiResult)
            return ['message' => 'Файл успешно удален.'];
        else {
            return response([
                'message' => 'Произошла ошибка при удалении файла.'
            ], 400);
        }
    }

    public function update(UpdateRequest $request, ApiFilesInterface $filesService): array|Application|ResponseFactory|Response
    {
        $filePath = $request->validated('id');
        $newFileName = $request->validated('name');

        try {
            $apiResult = $filesService->renameFile($filePath, $newFileName);
        } catch (Exception $e) {
            return response([
                'message' => $e->getMessage()
            ], 400);
        }

        if ($apiResult)
            return ['message' => 'Файл успешно изменен.'];
        else {
            return response([
                'message' => 'Произошла ошибка при изменении файла.'
            ], 400);
        }
    }

    public function download(DownloadRequest $request, ApiFilesInterface $filesService): array|Application|ResponseFactory|Response
    {
        $filePath = $request->validated('id');

        try {
            $apiResult = $filesService->downloadFile($filePath);
        } catch (Exception $e) {
            return response([
                'message' => $e->getMessage()
            ], 400);
        }

        return ['link' => $apiResult];
    }
}
