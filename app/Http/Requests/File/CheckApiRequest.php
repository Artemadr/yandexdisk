<?php

namespace App\Http\Requests\File;

use App\Rules\AllowedApiInstance;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class CheckApiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return Request::capture()->expectsJson() ? [
            'api' => [
                'required',
                'string',
                new AllowedApiInstance(),
            ]
        ] : [];
    }
}
