<?php

namespace App\Interfaces;

use Exception;
use Illuminate\Http\UploadedFile;

interface ApiFilesInterface
{
    /**
     * @throws Exception
     */
    public function uploadFile(UploadedFile $file): bool;

    /**
     * @throws Exception
     */
    public function getFiles(int $page, int $perPage): array;

    /**
     * @throws Exception
     */
    public function deleteFile($file): bool;

    /**
     * @throws Exception
     */
    public function renameFile($file, string $newName): bool;

    /**
     * @throws Exception
     */
    public function downloadFile($file): string;
}
