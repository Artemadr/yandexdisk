<?php

namespace App\Providers;

use App\Http\Requests\File\CheckApiRequest;
use App\Interfaces\ApiFilesInterface;
use App\Services\YandexApiFilesService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(CheckApiRequest $request)
    {
        $api = $request->validated('api');
        $apiClassName = '\App\Services\\' . $api . 'FilesService';

        $this->app->bind(ApiFilesInterface::class, $apiClassName);
    }
}
