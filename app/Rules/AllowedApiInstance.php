<?php

namespace App\Rules;

use App\Interfaces\ApiFilesInterface;
use Illuminate\Contracts\Validation\InvokableRule;

class AllowedApiInstance implements InvokableRule
{
    /**
     * Run the validation rule.
     *
     * @param string $attribute
     * @param mixed $value
     * @param \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString $fail
     * @return void
     */
    public function __invoke($attribute, $value, $fail)
    {
        $className = '\App\Services\\' . $value . 'FilesService';

        if (!is_subclass_of($className, ApiFilesInterface::class)) {
            $fail('Данное API не поддерживается.');
        }
    }
}
