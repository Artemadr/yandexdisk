<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\InvokableRule;
use Illuminate\Http\UploadedFile;

class RestrictedExtensions implements InvokableRule
{
    /**
     * Run the validation rule.
     *
     * @param string $attribute
     * @param mixed $value
     * @param \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString $fail
     * @return void
     */
    public function __invoke($attribute, $value, $fail)
    {
        if (!$value instanceof UploadedFile) {
            return;
        }

        $extensions = config('app.restricted_extensions');
        $extension = $value->getClientOriginalExtension();

        if (in_array($extension, $extensions)) {
            $fail('Файл не должен быть с расширениями: ' . implode(', ', $extensions) . '.');
        }

    }
}
