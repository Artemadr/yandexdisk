<?php

namespace App\Services;

use App\Interfaces\ApiFilesInterface;


/**
 * Класс-заглушка (Mock) просто для демонстрации работы интерфейсов API
 */
class NewApiFilesService implements ApiFilesInterface
{
    public function getFiles(int $page = 1, int $perPage = 20): array
    {
        return [];
    }

    public function uploadFile($file): bool
    {
        throw new \Exception('error');
    }

    public function deleteFile($file): bool
    {
        return false;
    }

    public function renameFile($file, string $newName): bool
    {
        return false;
    }

    public function downloadFile($file): string
    {
        return '';
    }
}
