<?php

namespace App\Services;

use App\Interfaces\ApiFilesInterface;
use Arhitector\Yandex\Disk;
use Exception;
use Illuminate\Pagination\Paginator;

class YandexApiFilesService implements ApiFilesInterface
{
    private Disk $disk;

    public function __construct()
    {
        $oAuthToken = config('app.oauth_token');
        $this->disk = new Disk($oAuthToken);
    }

    public function getFiles(int $page = 1, int $perPage = 20): array
    {
        $files = $this->getAllFiles();

        return $this->getPaginatedArray($files, 'index', $page, $perPage);
    }

    protected function getAllFiles(): array
    {
        $resources = $this->disk->getResources()
            ->setLimit(PHP_INT_MAX)
            ->setOffset(0);

        $files = [];

        foreach ($resources as $resource) {
            $arResource = $resource->toArray();

            $files[] = [
                'id' => $arResource['path'],
                'name' => $arResource['name'],
                'date' => \DateTimeImmutable::createFromFormat(DATE_ATOM, $arResource['modified'])->format('Y-m-d H:i:s'),
            ];
        }

        return $files;
    }

    protected function getPaginatedArray(array $array, $routeName = "index", int $page = 1, int $perPage = 20): array
    {
        $paginator = new Paginator(array_slice($array, ($page - 1) * $perPage), $perPage, $page, ["path" => route($routeName)]);

        $arPaginator = $paginator->toArray();
        $arPaginator['last_page'] = ceil(count($array) / $perPage);
        $arPaginator['last_page_url'] = route($routeName, ['page' => $arPaginator['last_page']]);
        $arPaginator['total'] = count($array);

        return $arPaginator;
    }

    public function uploadFile($file): bool
    {
        $resource = $this->disk->getResource('disk:/' . $file->getClientOriginalName());

        if ($resource->has()) {
            throw new Exception("Файл с таким названием уже был загружен.");
        }

        $resource->upload($file->getRealPath());

        return true;
    }

    public function deleteFile($file): bool
    {
        $resource = $this->disk->getResource($file);
        if (!$resource->has())
            return false;

        $apiStatus = $resource->delete();

        return match ($apiStatus) {
            true => true,
            default => false,
        };
    }

    public function renameFile($file, string $newName): bool
    {
        $resource = $this->disk->getResource($file);
        if (!$resource->has())
            return false;

        $apiStatus = $resource->move(dirname($file) . '/' . $newName);

        return match ($apiStatus) {
            true => true,
            default => false,
        };
    }

    public function downloadFile($file): string
    {
        $resource = $this->disk->getResource($file);
        if (!$resource->has())
            throw new Exception('Файл не найден.');

        $resourceInfo = $resource->toArray();

        if (!isset($resourceInfo['file']) || strlen($resourceInfo['file']) == 0)
            throw new Exception('Не удалось найти ссылку для скачивания.');

        return $resourceInfo['file'];
    }

}
