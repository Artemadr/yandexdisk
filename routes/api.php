<?php

use App\Http\Controllers\FilesController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'files'], function () {
    Route::get('/', FilesController::class . '@index')->name('index');
    Route::delete('/', FilesController::class . '@delete');
    Route::patch('/', FilesController::class . '@update');
    Route::post('/', FilesController::class . '@store');
    Route::get('/download', FilesController::class . '@download');
});
